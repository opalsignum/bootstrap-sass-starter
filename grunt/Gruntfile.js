module.exports = function(grunt) {
    grunt.initConfig({
        watch: {
            sass: {
                files: ['../develop/htdocs/sass/**/*.scss'],
                tasks: ['sass']
            },
            styles: {
                files: ['../develop/htdocs/css/style.css'],
                tasks: ['cssmin']
            }
        },
        sass: {
            dev: {
                files: {
                    '../develop/htdocs/css/style.css': '../develop/htdocs/sass/style.scss'
                }
            }
        },
        cssmin: { // minifying css task
            dist: {
                files: {
                    '../deploy/css/style.min.css': '../develop/htdocs/css/style.css'
                }
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        '../develop/htdocs/css/*.css',
                        '../develop/htdocs/js/*.js',
                        '../develop/htdocs/*.html'
                    ]
                },
                options: {
                    watchTask: true,
                    injectChanges: true,
                    server: {
                        baseDir: "../develop/htdocs"
                    }
                },
                browser: ["google chrome", "firefox"]
            }
        }

    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    //grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');

    grunt.registerTask('default', ['browserSync', 'watch']);
};